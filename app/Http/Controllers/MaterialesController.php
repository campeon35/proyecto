<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Material;
use App\Utiliza;

class MaterialesController extends Controller
{
    public function getTodos() {
		$materiales = Material::all();
		return view('materiales.index', array('materiales' => $materiales));
	}

	public function getVer($id) {
    	$material = Material::findOrFail($id);
		return view('materiales.descripcion', array('material' => $material));
	}

	//Llevará al documento del material que sera añadido a parte (.php .html pdf...)
	public function getDocumento($id) {
    	$material = Material::findOrFail($id);
    	
    	//Conseguir el usuario logueado y el material visitado para añadir la relacion a la tabla "utiliza"
		$usuario = auth()->user(); 
		$material = Material::findOrFail($id);

		$u = new Utiliza();

		$u->usuario_id = $usuario->id;
		$u->material_id = $material->id;

		try{
			$u->save();
			//No implementados los ficheros de los materiales
			return view('materiales.{$material->nombre}', array('material' => $material));

		} catch(\Illuminate\Database\QueryException $ex){
			return redirect('materiales')->with('mensaje', "Error");
		}
	}

	public function getCrear() {
		return view('materiales.crear');
	}
	
	public function postCrear(Request $request) {
		$m = new Material();

		$m->id = $request->id;
		$m->nombre = $request->nombre;
		$m->tipo = $request->tipo;
		$m->lenguajes = $request->lenguajes;
		
		if (!empty($request->imagen) && $request->imagen->isValid()) {
			$m->imagen = $request->imagen->store('', 'materiales');
		}
		try{
			$m->save();
			return redirect('materiales')->with('mensaje', "Material: $m->nombre guardado");

		} catch(\Illuminate\Database\QueryException $ex){
			return redirect('materiales')->with('mensaje', "Error en la creación del material");
		}
	}
}
