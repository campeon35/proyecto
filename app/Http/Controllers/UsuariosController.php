<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    public function getDatos() {
    	$usuario = auth()->user();
    	return view('usuarios.usuario', array('usuario' => $usuario));
    }
}
