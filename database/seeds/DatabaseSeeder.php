<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Material;
use App\Utiliza;

class DatabaseSeeder extends Seeder
{

	private $materiales = array(
		array(
            'nombre' => "Bucles_basicos",
			'tipo' => "Ejercicio",
			'lenguajes' => "PHP",
			'imagen' => null
		),
		array(
            'nombre' => "Juego_utilizando_eventos",
			'tipo' => "Juego",
			'lenguajes' => "JavaScript-HTML-CSS",
			'imagen' => "juegoJS.png"
		)
	);

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	self::seedUsuarios();
    	//$this->command->info('Tabla users inicializada con datos');
    	self::seedMaterial();
    	//$this->command->info('Tabla materiales inicializada con datos');
    	self::seedUtiliza();     //Hacer después de los 2 anteriores
    	//$this->command->info('Tabla utiliza inicializada con datos');
        // $this->call(UsersTableSeeder::class);
    }

    public function seedUsuarios(){
    	DB::table('users')->delete();
    	$u = new User();
	 	$u->name = "David";
	 	$u->email = "david_barcelona-10@hotmail.com";
	 	$u->password = bcrypt('a');
	 	$u->save();
    }

    public function seedMaterial(){
    	DB::table('materiales')->delete();
    	foreach ($this->materiales as $material) {
		 	$m = new Material();
            $m->nombre = $material['nombre'];
		 	$m->tipo = $material['tipo'];
		 	$m->lenguajes = $material['lenguajes'];
		 	if ($material['imagen'] != null) {
		 		$m->imagen = $material['imagen'];
		 	}
		 	$m->save();
		}
    }

    //Puede dar error si no hay usuario o material con id=1
    public function seedUtiliza(){
    	DB::table('utiliza')->delete();
    	$ut = new Utiliza();
	 	$ut->usuario_id = 1;
	 	$ut->material_id = 1;
	 	$ut->save();
    }
}
