<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'InicioController@getInicio');
Route::get('materiales', 'MaterialesController@getTodos');
Route::get('materiales/ver/{id}', 'MaterialesController@getVer');

Route::group(['middleware' => 'auth'], function() {

	Route::get('materiales/utilizar/{id}', 'MaterialesController@getDocumento');
	Route::get('materiales/crear', 'MaterialesController@getCrear');
	Route::post('materiales/crear', 'MaterialesController@postCrear');
	Route::get('usuario', 'UsuariosController@getDatos');

});