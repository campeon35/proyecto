@extends('layouts.master')

@section('titulo')
	Materiales
@endsection

@section('contenido')

	@if(session("mensaje"))
		<h4>{{ session("mensaje") }}</h4>
	@else
		<h2>Todos los materiales disponibles:</h2>
	@endif
	
	<div class="row">
		@foreach($materiales as $clave => $material)
			<div class="col-xs-12 col-sm-6 col-md-4">
				<a href="{{ url('/materiales/ver/' . $material->id ) }}">
					<img src="assets/imagenes/{{ $material->imagen }}" style="height:200px"/>
					<h4 style="min-height:45px; margin:5px 0 10px 0">
						{{ $material->nombre }}
					</h4>
				</a>
				<div>
					<h5>{{ $material->tipo }}</h5>
					<p>Realizado con los lenguajes {{ $material->lenguajes }}</p>
				</div>
			</div>
		@endforeach
	</div>
@endsection