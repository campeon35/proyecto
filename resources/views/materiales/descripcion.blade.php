@extends('layouts.master')

@section('titulo')
	Material {{ $material->imagen }}
@endsection

@section('contenido')
	<div class="row">
		<div class="col-sm-3">
			<img src="../../assets/imagenes/{{ $material->imagen }}" height="350px" width="350px">
		</div>
		<div class="col-sm-9">
			<h1>{{ $material->nombre }}</h1>
			<h4>Tipo: {{ $material->tipo }}</h4>
			<p>Lenguajes utilizados: {{ $material->lenguajes }}</p>
			<a class="btn btn-warning" href="../utilizar/{{ $material->id }}" role="button">Entrar</a>
			<a class="btn btn-outline-dark" href="../../" role="button">Volver al listado</a>
		</div>
	</div>
@endsection