@extends('layouts.master')

@section('titulo')
	Nuevo material
@endsection

@section('contenido')
	<div class="row">
		<div class="offset-md-3 col-md-6">
			<div class="card">
				<div class="card-header text-center">
					Añadir material
				</div>
				<div class="card-body" style="padding:30px">
					<form action="{{ action('MaterialesController@postCrear') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" name="nombre" id="nombre" class="form-control">
						</div>
						<div class="form-group">
							<label for="tipo">Tipo</label>
							<input type="text" name="tipo" id="tipo" class="form-control">
						</div>
						<div class="form-group">
							<label for="lenguajes">Lenguajes</label>
							<input type="text" name="lenguajes" id="lenguajes" class="form-control">
						</div>
						<div class="form-group">
							<label for="imagen">Imagen</label>
							<br>
							<input type="file" name="imagen" id="imagen">
						</div>
						<div class="form-group text-center">
							<button type="submit" class="btn btn-success" style="padding:8px 100px; margin-top:25px;">
								Añadir material
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection