@extends('layouts.master')

@section('titulo')
	Usuario {{ $usuario->nombre }}
@endsection

@section('contenido')
	<div class="row">
		<div class="col-sm-3">
			<!--No implementadas las imágenes de usuarios -->
			<img src="../../assets/imagenes/usuarios{{ $usuario->imagen }}" height="350px">
		</div>
		<div class="col-sm-9">
			<h3>{{ $usuario->name }}</h3>
			<h4>Email: {{ $usuario->email }}</h4>
			<p>Perfil creado el: {{ $usuario->created_at }}</p>
			<br><br>
			<h6>Materiales usados:</h6>
			@foreach ($usuario->materiales as $material) 
    			<p> id: {{ $material->id }}, nombre: {{ $material->nombre }} </p>
			@endforeach
			<a class="btn btn-warning" href="editar/{{ $usuario->id }}" role="button">Editar (no implementado)</a>
			<a class="btn btn-outline-dark" href="." role="button">Volver al principio</a>
		</div>
	</div>
@endsection